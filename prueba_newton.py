from calculador import raiz_newton
##Primerodejamos al usuario poner las variables para encontrar pero las transformamos en floats para que nuestros resultados esten con decimales.
##Para los errores en este problema vemos que que los errores podemos controlarlos con if's
x = float(raw_input("Escriba un numero positivo unicamente: "))
err = float(raw_input("Ingrese el err: "))
if err>1:
    "Escriba una tolerancia menor que 1."
    exit()
if x>0:
    z = raiz_newton(x,err)
    print "La raiz de ", x, " es ", z, " con tolerancia de ", err
else:
    print "Por favor escriba un numero positivo y diferente de cero"
    exit()