
def wallis(n):
    ##wallis se encargara de hacer la division
    a1 = float(2*n)
    a2 = float(a1-1)
    a3 = float(a1+1)
    yoquese = (a1 * a1)/(a2 * a3)
    return yoquese
def calcular_pi(nmax):
    ## Y este metodo se encargara de la productoria dada por un numero dado.
    i = 1
    b = 1
    while i <= nmax:
        b = b*wallis(i)
        i+=1
    return b
def raiz_newton(x,err):
    ##Este metodo vemos que se encargara de hacer las iteraciones para calcular la raiz cuadrada
    i = x/2 ##Aqui hacemos la primera aproximacion tomamos esto por facilidad
    dif = 1
    while dif>err: ##aqui tomaremos err como la convergencia que deseamos para nuestro resultado, ya que cuando dif=err parara el ciclo.
        c = float(x/i)
        d = (c+i)/2
        dif = i-d
        i = d
    return i