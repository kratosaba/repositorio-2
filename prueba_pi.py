# -*- coding: utf-8 -*-
import math
import sys
from calculador import calcular_pi
##Comenzamos pensando el los posibles errores que podemos encontrarnos en este problema, los unicos que podemos son
##hechos por el usuario, y estos podemos manejarlo con if's.
if len(sys.argv)==1:
    print "Error ponga un parametro"
    exit()
N = sys.argv[1]## Nombramos la variable dada por el usuario desde la lista sys.argv
if  str.isdigit(N):
    ## en este bloque de codigo corrovoramos que el usuario haya puesto un numero.
        B = int(N)
        pi = 2*calcular_pi(B)
        pi2 = math.pi
        print "Mi valor es:", pi, "El valor de la clase math es: ", pi2, "Y la diferencia es: ", pi-pi2
else:
    print "Escriba un numero"
    exit()